package pl.car.carscheduler.config;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class ApplicationConstants {

    public static final String CORRELATION_ID = "correlation-id";
    public static final String AUTHORIZATION = "Authorization";
    public static final String BEARER = "Bearer ";

}
