package pl.car.carscheduler.config;

import com.google.common.collect.Lists;
import feign.RequestInterceptor;
import feign.RequestTemplate;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;
import pl.car.carscheduler.security.TokenProvider;
import pl.car.filter.UserContextHolder;

import java.util.Objects;

import static pl.car.carscheduler.config.ApplicationConstants.AUTHORIZATION;
import static pl.car.carscheduler.config.ApplicationConstants.BEARER;

@Component
@RequiredArgsConstructor
public class MicroservicesProxyAuthenticationInterceptor implements RequestInterceptor {

    private static final String SYSTEM_ROLE = "ROLE_SYSTEM";
    private static final String USERNAME = "system";
    private static final String PASSWORD = "system";

    private final TokenProvider tokenProvider;

    @Override
    public void apply(RequestTemplate requestTemplate) {
        String correlationId = UserContextHolder.getContext().getCorrelationId();
        if (Objects.nonNull(correlationId) && !correlationId.isEmpty()) {
            requestTemplate.header(ApplicationConstants.CORRELATION_ID, correlationId);
        }

        String authToken = UserContextHolder.getContext().getAuthToken();
        if (Objects.nonNull(authToken) && !authToken.isEmpty()) {
            requestTemplate.header(AUTHORIZATION, BEARER + authToken);
            return;
        }
        if (Objects.nonNull(UserContextHolder.getContext().getAuthToken()) && !UserContextHolder.getContext().getAuthToken().isEmpty()) {
            requestTemplate.header(AUTHORIZATION, UserContextHolder.getContext().getAuthToken());
            return;
        }
        if(!requestTemplateHasToken(requestTemplate)) {
            requestTemplate.header(AUTHORIZATION, generateToken());
        }
    }

    private boolean requestTemplateHasToken(RequestTemplate requestTemplate) {
        return !requestTemplate.headers().get(AUTHORIZATION).isEmpty();
    }

    private String generateToken() {
        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(USERNAME, PASSWORD,
                Lists.newArrayList(new SimpleGrantedAuthority(SYSTEM_ROLE)));
        return BEARER + tokenProvider.generateJwtToken(usernamePasswordAuthenticationToken);
    }

}
