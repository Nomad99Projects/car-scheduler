package pl.car.carscheduler.api.greeting.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/greeting")
public class GreetingController {

    @GetMapping
    public ResponseEntity<String> getSchedulerGreeting() {
        return ResponseEntity.ok("Greeting from CAR-Scheduler microservice!");
    }
}
